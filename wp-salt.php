<?php
/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 */
define('AUTH_KEY',         '4zDfgd33fas18sGD2R5qTe0jJ1AtoXpwprCvCf1fGH8v4y3VVzvWfcbEjIcRpbA6');
define('SECURE_AUTH_KEY',  'W97rcJ5LpuwFgF8FgW8w5dFqPMgdLd8tb46fojqjKm3uhqGc61ULSGACXR186quu');
define('LOGGED_IN_KEY',    'Ha4Jd0IeeNBdi2GXIhGAfD8d8rHje5ta6JSvQ34Q4REULgEWGW7jxFXmpFMALhow');
define('NONCE_KEY',        'X425UhAdnrsdWnEIIGxutynruL3x8mznp0FVsuEL4ummaBr1bsto3ubczTSocbtP');
define('AUTH_SALT',        'q6qVdSv2Fr8fYEPjiVqCA8NaSiwVFtQ7WNW7nVcB86faPJeSfwdJ1GosoHzMQq5W');
define('SECURE_AUTH_SALT', 'N4p1jLNFE7eoMsWMaVSIjDhqAmg4avDQFvbmr00C7w5ngEnrs60mIoHiRIhYPtY2');
define('LOGGED_IN_SALT',   'psmSXB7MD07YPzeARCUqJnBJjs26wYUumngN6IxNyJKwEQhyeSyKQdvUSctQ6Uad');
define('NONCE_SALT',       '9KD6tb1VBNbqKGvmpP3u7II0BBY571SSJcWXTLVx2y7BJsNSmA4gp5X8N1SyMHrV');
