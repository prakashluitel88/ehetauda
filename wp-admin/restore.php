<?php

//Start downloading the backup file from source server
if ($_REQUEST['action'] == 'transfer') {
    addLog('calling url wget -b ' . urldecode($_REQUEST['ref']) . '/wordpress_site_backup.zip');
    @unlink('wordpress_site_backup.zip');
    @unlink('wordpress_db_backup.sql');
    @unlink('wget-log');
    exec('wget -b ' . urldecode($_REQUEST['ref']) . '/wordpress_site_backup.zip ', $output);
    //var_dump($output);
    exit;
}

//Send the progress of file download
if ($_REQUEST['action'] == 'progress') {
    echo `egrep -o "[0-9]+%" wget-log | tail -1`;
    exit;
}
addLog('starting');
//Start the restore process

@ini_set('memory_limit', '512M');
@set_time_limit(0);
///////  Initialize Framework
require_once(dirname(__FILE__) . '/wp-load.php');

//  Verifications
if (file_exists('wordpress_site_backup.zip')) {

    if (exec("unzip -o wordpress_site_backup.zip settings.txt wordpress_db_backup.sql 2>&1", $output)) {
        //echo '<br> <b>Matching Versions of Source & Destination</b>';
    } else {
        echo "<strong style='color:red;'>command has been disabled by host</strong>";
        addLog(print_r($output, true));
        //exit();
    }

    addLog('url restored');

    $config_array = unserialize(file_get_contents('settings.txt'));
    if ($config_array['version'] != $wp_version) {
        //echo displayMsg('Source & Destination Versions Are Different','error');
        //exit();
    } else {
        //echo displayMsg("Source($config_array[version]) & Destination($wp_version) Versions Are Matched", 'ok');
    }

    if (!file_exists('wordpress_db_backup.sql')) {
        addLog('Database backup file not exist to import');
        echo displayMsg('Database backup file not exist to import', 'error');
        exit();
    }

    if (exec("unzip -o wordpress_site_backup.zip -x settings.txt wordpress_db_backup.sql wp-config.php 2>&1", $output)) {
        addLog('Tar/Archive file uncompressed successfully');
        echo displayMsg('Tar/Archive file uncompressed successfully', 'ok');
    } else {
        echo displayMsg('Command has been disabled by host', 'error');
        addLog(print_r($output, true));
    }
} else {
    echo displayMsg('Archive Does Not Exist', 'error');
    exit();
}


//  Import Database
if (file_exists('wordpress_db_backup.sql')) {
    $username = DB_USER;
    $password = DB_PASSWORD;
    $hostname = DB_HOST;
    $database = DB_NAME;

    $dbc = @mysql_connect($hostname, $username, $password);
    mysql_query("SET NAMES 'utf8'");
    if (!$dbc OR ! mysql_select_db($database)) {
        //echo '<p class="error">The site is currently experiencing technical difficulties. We apologize for any inconvenience.</p>';
        echo displayMsg('The site is currently experiencing technical difficulties. We apologize for any inconvenience.', 'error');
        exit();
    }

    $sql = "SHOW TABLES FROM $database";
    if ($result = mysql_query($sql)) {
        /* add table name to array */
        while ($row = mysql_fetch_row($result)) {
            $found_tables[] = $row[0];
        }
    } else {
        die("Error, could not list tables. MySQL Error: " . mysql_error());
    }

    /* loop through and drop each table */
    if ($found_tables) {
        echo displayMsg('Deleting Current Database Tables .........', 'ok');
        foreach ($found_tables as $table_name) {
            $sql = "DROP TABLE $database.$table_name";
            if ($result = mysql_query($sql)) {
                echo "Success - table $table_name deleted.<br>";
            } else {
                echo "Error deleting $table_name. MySQL Error: " . mysql_error() . "";
            }
        }
    }

    $command = "mysql -u$username -p$password -h$hostname $database < wordpress_db_backup.sql";
    system($command, $result);
    echo displayMsg('Imported Database Successfully', 'ok');
}
addLog('before prefix updating ' . $config_array['table_prefix']);
updateTablePrefix($config_array['table_prefix']);
addLog('before url update');
updateOldLinks(trim($config_array['site_url'], '/'), trim('http://wordpress-10006-22225-63913.cloudwaysapps.com/', '/'), $config_array['table_prefix']);

///////  Delete Files Of No Use
unlink('wordpress_site_backup.zip');
unlink('wordpress_db_backup.sql');
unlink('restore.php');
unlink('wget-log');
unlink('restore_log.log');

/*
 * Update links in DB in posts, wp-options and few other tables
 */

function updateOldLinks($oldurl, $newurl, $prefix) {
    if (!startsWith($newurl, 'http://'))
        $newurl = 'http://' . $newurl;

    addLog($oldurl);
    addLog($newurl);
    define('SHORTINIT', true);
    require_once( $_SERVER['DOCUMENT_ROOT'] . '/wp-load.php' );

    global $wpdb;
    $results = array();
    $queries = array(
        'content' => array('UPDATE ' . $prefix . "posts SET post_content = replace(post_content, %s, %s)", __('Content Items (Posts, Pages, Custom Post Types, Revisions)', 'velvet-blues-update-urls')),
        'excerpts' => array('UPDATE ' . $prefix . "posts SET post_excerpt = replace(post_excerpt, %s, %s)", __('Excerpts', 'velvet-blues-update-urls')),
        'attachments' => array('UPDATE ' . $prefix . "posts SET guid = replace(guid, %s, %s) WHERE post_type = 'attachment'", __('Attachments', 'velvet-blues-update-urls')),
        'links' => array('UPDATE ' . $prefix . "links SET link_url = replace(link_url, %s, %s)", __('Links', 'velvet-blues-update-urls')),
        'custom' => array('UPDATE ' . $prefix . "postmeta SET meta_value = replace(meta_value, %s, %s)", __('Custom Fields', 'velvet-blues-update-urls')),
        'guids' => array('UPDATE ' . $prefix . "posts SET guid = replace(guid, %s, %s)", __('GUIDs', 'velvet-blues-update-urls')),
        'options' => array('UPDATE ' . $prefix . "options SET option_value = replace(option_value,%s,%s)", '')
    );
    $options = array('content', 'excerpts', 'links', 'attachments', 'custom', 'guids', 'options');
    foreach ($options as $option) {
        if ($option == 'custom') {
            $n = 0;
            $row_count = $wpdb->get_var("SELECT COUNT(*) FROM " . $prefix . "postmeta");
            $page_size = 10000;
            $pages = ceil($row_count / $page_size);

            for ($page = 0; $page < $pages; $page++) {
                $current_row = 0;
                $start = $page * $page_size;
                $end = $start + $page_size;
                $pmquery = "SELECT * FROM " . $prefix . "postmeta WHERE meta_value <> ''";
                $items = $wpdb->get_results($pmquery);
                foreach ($items as $item) {
                    $value = $item->meta_value;
                    if (trim($value) == '')
                        continue;

                    $edited = VB_unserialize_replace($oldurl, $newurl, $value);

                    if ($edited != $value) {
                        $fix = $wpdb->query('UPDATE ' . $prefix . "postmeta SET meta_value = '" . $edited . "' WHERE meta_id = " . $item->meta_id);
                        if ($fix)
                            $n++;
                    }
                }
            }
            $results[$option] = array($n, $queries[$option][1]);
        }
        else {
            addLog($wpdb->prepare($queries[$option][0], $oldurl, $newurl));
            $result = $wpdb->query($wpdb->prepare($queries[$option][0], $oldurl, $newurl));
            $results[$option] = array($result, $queries[$option][1]);
        }
    }
    addLog(print_r($results, TRUE));
    return $results;
}

function VB_unserialize_replace($from = '', $to = '', $data = '', $serialised = false) {
    try {
        if (is_string($data) && ( $unserialized = @unserialize($data) ) !== false) {
            $data = VB_unserialize_replace($from, $to, $unserialized, true);
        } elseif (is_array($data)) {
            $_tmp = array();
            foreach ($data as $key => $value) {
                $_tmp[$key] = VB_unserialize_replace($from, $to, $value, false);
            }
            $data = $_tmp;
            unset($_tmp);
        } else {
            if (is_string($data))
                $data = str_replace($from, $to, $data);
        }
        if ($serialised)
            return serialize($data);
    } catch (Exception $error) {
        
    }
    return $data;
}

function displayMsg($msg, $type) {
    return "<p class='message-box $type'>$msg</p>";
}

function addLog($line) {
    file_put_contents('restore_log.log', $line . "\n", FILE_APPEND);
}

/*
 * Update Table Prefix in config file.
 */

function updateTablePrefix($prefix) {
    $contents = str_replace('$table_prefix  = \'wp_\';'
            , '$table_prefix  = \'' . $prefix . '\';'
            , file_get_contents('wp-config.php'));
    //addLog('File Contents');
    //addLog($contents);
    file_put_contents('wp-config.php', $contents);
}

function startsWith($haystack, $needle) {
    $length = strlen($needle);
    return (substr($haystack, 0, $length) === $needle);
}

?>
