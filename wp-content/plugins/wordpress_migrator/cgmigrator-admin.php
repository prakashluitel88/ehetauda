<?php
//error_reporting(E_ALL);
include( plugin_dir_path(__FILE__) . 'cg_functions.php');
?>
<link rel="stylesheet" type="text/css" href="<?php echo plugins_url('/assets/style.css', __FILE__); ?>" />

<script type="text/javascript">

    jQuery("#migrate_now_btn").live("click", function() {
        jQuery('#mgr_stps_demo_desc').css('display', 'none');
        jQuery(this).css('display', 'none');
        jQuery("#mgr_stps_new .mgr_stp_main").css('display', 'block');
    });


<?php if (!empty($_POST)) { ?>
        jQuery("#migrate_now_btn").trigger('click');
<?php } else { ?>
        jQuery("#mgr_stps_new .mgr_stp_main").css('display', 'none');
        jQuery('#migrate_now_btn').css('display', 'block');
        jQuery('#mgr_stps_demo_desc').css('display', 'block');
<?php } ?>

<?php if (!empty($errors)) { ?>
        jQuery('#migrate_now_btn').css('display', 'none');
<?php } ?>
    // forms setup

    var server_name_previous = '';
    // clear input on focus
    jQuery('#server_name').focus(function()
    {
        if (jQuery(this).val() == jQuery(this).attr('title'))
        {
            server_name_previous = jQuery(this).val();
            jQuery(this).val('');
        }
    });

    // if field is empty afterward, add text again
    jQuery('#server_name').blur(function()
    {
        if (jQuery(this).val() == '')
        {
            jQuery(this).val(server_name_previous);
        }
    });

    var user_name_previous = '';
    // clear input on focus
    jQuery('#user_name').focus(function()
    {
        if (jQuery(this).val() == jQuery(this).attr('title'))
        {
            user_name_previous = jQuery(this).val();
            jQuery(this).val('');
        }
    });

    // if field is empty afterward, add text again
    jQuery('#user_name').blur(function()
    {
        if (jQuery(this).val() == '')
        {
            jQuery(this).val(user_name_previous);
            jQuery('#directory_path').val('');
        } else {
            //jQuery('#directory_path').val("/var/www/"+jQuery(this).val()+"/htdocs");
            jQuery('#directory_path').val("/public_html/");
        }
    });

    var password_previous = '';
    // clear input on focus
    jQuery('#password').focus(function()
    {
        if (jQuery(this).val() == jQuery(this).attr('title'))
        {
            password_previous = jQuery(this).val();
            jQuery(this).val('');
        }
    });

    // if field is empty afterward, add text again
    jQuery('#password').blur(function()
    {
        if (jQuery(this).val() == '')
        {
            jQuery(this).val(password_previous);
        }
    });

    var directory_path_previous = '';
    // clear input on focus
    jQuery('#directory_path').focus(function()
    {
        if (jQuery(this).val() == jQuery(this).attr('title'))
        {
            directory_path_previous = jQuery(this).val();
            jQuery(this).val('');
        }
    });

    // if field is empty afterward, add text again
    jQuery('#directory_path').blur(function()
    {
        if (jQuery(this).val() == '')
        {
            jQuery(this).val(directory_path_previous);
        }
    });

</script>
<style>
    #progress{
        background-color:#474747;
        width:600px;
        height:20px;
        position:relative;
        leftx:200px;
        marginx:3px 0;
        margin:0 auto;
    }

    img{
        position:absolute;
        left:0px;
        top:0px;
        z-index:100;
    }

    .mgr_stps_new {
        padding-bottom:10px;
    }

</style>
<div class="mgr_main">
    <div class="mgr_top">
        <a href="javascript:void(0);" class="hd_logo"></a>
        <div class="mgr_top_rt">For Support: <a href="mailto:support@cloudways.com?cc=aaqib@cloudways.com">support@cloudways.com</a></div>
    </div>
    <div class="mgr_nw_hd">
        <h1>Cloudways Migrator <sup>(TM)</sup></h1><br />
        <span>Migrate your complete WordPress site in 3 easy steps.</span>
    </div>


    <?php
    if (!empty($errors)) {
        echo "<br clear='all'>";
        echo "<div style='margin:3px 0;font-size:13px;border:1px dashed #333;padding:5px;text-align:left;color:darkred;'>";
        echo "<span> Following System Errors are Found, Please Correct These First Before Continue </span><br>";
        foreach ($errors as $error) {
            echo "<span><em> *$error </em></span><br>";
        }
        echo "</div>";
    }
    ?>

    <!--Migrator Steps Demo-->
    <div class="mgr_stps_new" id="mgr_stps_demo" style="display:block">

        <div id="mgr_stps_demo_desc" style='display:none;'>
            <div class="mgr_stp_main">
                <div class="mgr_stp1">
                    <h3>Backup</h3>
                    <span>Create a backup of your existing WordPress Website.You don't need to copy files and DB separately, as the WP Migrator will create a single backup of site files,DB/tables, images, plug-ins and themes.</span>
                </div>
            </div>
            <div class="mgr_stp_main">
                <div class="mgr_stp1 mgr_stp2">
                    <h3>Move</h3>
                    <span>Enter your Cloudways FTP credentials, along with target Directory path, and the WP Migrator will start importing the backup files into the new server.</span>
                </div>
            </div>
            <div class="mgr_stp_main mgr_stp_main2">
                <div class="mgr_stp1 mgr_stp3">
                    <h3>Restore</h3>
                    <span>Activate your backup in the Cloudways environment, without having to use phpMyAdmin or MySQL Workbench.Bingo! Your WordPress will be fully functional.</span>
                </div>
            </div>
        </div>

        <div class="mgr_demo_btm">
            <div class="mgr_demo_btm_lft" style='display:none;'>
                <a href="javascript:void(0);" class="mgr_vd_lnk"></a>
                <div>
                    <span>Do you want to checkout in motion?</span><br clear="all" />
                    <a href="javascript:void(0);">&raquo; Play Video</a>
                </div>
            </div>
            <div class="mgr_demo_btm_rt">
                <a href="#mgr_stps_new" id="migrate_now_btn" class="round_6" style='display:none;'>Start WordPress Migration</a>
            </div>
        </div>

    </div>

    <!--Migrator Steps Demo End-->		

    <!--Migrator Steps-->
    <div id="mgr_stps_new" class="mgr_stps_new" style="display:block;">

        <div class="mgr_stp_main">
            <div id="backup" class="mgr_stp1">
                <h2>Backup</h2>
                <span>Click on "Create Backup" to get started.</span>
                <a href="javascript:void(0);" class="round_6 btn-backup"><strong></strong><i>Create Backup</i></a><br clear="all" />
                <span><i>Note:</i> for large backups It may take a while, please be patient while process completes.</span>
            </div>
            <div id="backup_done" class="mgr_stp1_don" style="display:none;"></div>
        </div>

        <div class="mgr_stp_main">
            <div id="move" class="mgr_stp1 mgr_stp2 opacity">
                <h2>Move</h2>
                <form  class="mig-form" method="post" enctype="multipart/form-data">
                    <div class="mgr_stp2_in round_6 focus">
                        <input type="text" class="round_6" id="server_name" name="server_name" title="Destination URL (eg. http://example.cloudwaysapps.com)" placeholder="Destination URL (eg. http://example.cloudwaysapps.com)" value="<?php
                        if (isset($_POST['server_name'])) {
                            echo $_POST['server_name'];
                        }
                        ?>"/></div>
                    <div class="mgr_stp2_in round_6"><input type="text" class="round_6" id="user_name" name="user_name" title="username" placeholder="username" value="<?php
                        if (isset($_POST['user_name'])) {
                            echo $_POST['user_name'];
                        }
                        ?>" /></div>
                    <div class="mgr_stp2_in round_6"><input type="password" class="round_6" id="password" name="password" title="password" placeholder="password" value="<?php
                        if (isset($_POST['password'])) {
                            echo $_POST['password'];
                        }
                        ?>" /></div>
                    <div class="mgr_stp2_in round_6"><input type="text" class="round_6" id="directory_path" name="directory_path" title="directory path" value="<?php
                        if (isset($_POST['directory_path'])) {
                            echo $_POST['directory_path'];
                        } else {
                            echo '/public_html/';
                        }
                        ?>" /></div>
                    <input type="button" class="button round_6 btn-move" style="height:60px;" value="Move It Now" />
                    <input type="hidden" id="task" name="task" value="" />
                    <input type="hidden" id="debug" name="debug" value="<?php echo $debug; ?>" />
                    <input type="hidden" id="file" name="file" value="<?php echo $file; ?>" />
                </form>
            </div>
            <div id="move_done" class="mgr_stp2_don"  style="display:none;" ></div>
        </div>

        <div class="mgr_stp_main mgr_stp_main2">
            <div id="restore" class="mgr_stp1 mgr_stp3 opacity">
                <h2>Restore</h2>
                <span>Click on "Restore" to activate your WordPress install.</span>
                <a href="javascript:void(0);" class="round_6 btn-restore"><strong></strong><i>Restore</i></a><br clear="all" />
                <div class="mgr_stp3_lnk"><a href="javascript:void(0);">&lsaquo; Go back to previous step</a></div>
                <span><i>Note:</i> Please wait a while for the process to complete.</span>
            </div>
            <div id="restore_done" class="mgr_stp2_don"  style="display:none;" ></div>
        </div>

    </div>
    <!-- LOGIC -->
    <br clear="all">

    <div id="rs">
        <?php
        extract($_POST);
        extract($_GET);

        if ($task == 'backup_exist') {
            ob_end_clean();
            if (file_exists("../wordpress_site_backup.zip")) {
                echo 'yes';
            } else {
                echo 'no';
            }
            unlink('../wordpress_site_backup.zip');
            exit();
        }

        if ($task == 'process_backup') {
            Run_Process_Backup();
        }

        if ($task == 'process_transfer') {
            if (!file_exists("../wordpress_site_backup.zip")) {
                echo displayMsg('Please Run Backup First Before Moving', 'error');
                exit();
            }

            if (!_iscurlinstalled()) {
                echo displayMsg('Error:Either CURL is Disabled Or Not Installed', 'error');
                exit();
            }
            include( plugin_dir_path(__FILE__) . 'lib/Net/SFTP.php');
            createRestoreFile($_REQUEST['server_name']);
            transferFile('restore.php', $_REQUEST['server_name'], $_REQUEST['user_name'], $_REQUEST['password'], $_REQUEST['directory_path']);
            callRemoteURL('http://' . validateServerURL($_REQUEST['server_name']) . '/restore.php?action=transfer&ref=' . urlencode(get_home_url()));
            ?>
            <div id="progress">
                <img src="<?php echo plugins_url('/assets/sample.gif', __FILE__); ?>" height="20" class="im" width="0%">
                <script>
                    window.backupMoveInProgress = true;
                    window.serverIP = '<?php echo $_POST['server_name'] ?>';
                </script>
            </div>
            <?php
        }

        if ($task == 'process_restore') {
            runRestoreProcess($_POST);
            unlink('../wordpress_site_backup.zip');
        }
        ?>
    </div>		
    <!-- LOGIC End -->		

    <!--Migrator Steps-->

    <div class="mgr_nw_ft">
        <div class="mgr_nw_ft_lft">
            <a href="http://www.cloudways.com/en/wordpress-migrator.php">Learn More</a>
            <span>-</span>
            <a href="http://www.cloudways.com/en/contact_us.php">Contact us</a>
        </div>
        <div class="mgr_nw_ft_rt">
            <a href="mailto:join@cloudways.com">join@cloudways.com</a><br />
            <span>© 2009-<?php echo date("Y"); ?> Cloudways Ltd.<br />All rights reserved</span>
        </div>
    </div>
    <br clear="all" />
</div>


<br clear="all" />

<script type="text/javascript" >
    jQuery(document).ready(function() {
        if (window.backupMoveInProgress) {
            jQuery('#backup').addClass('opacity');
            checkProgress();
            jQuery('.im').after('<br style="clear:both;" /><p class="message-box ok">Transfer of backup files in progress ...</p>');
            sendLog('transfer');
        } else {
            sendLog('backup');
        }
        jQuery(".btn-backup").live("click", function() {
            var data = {
                action: 'backup_exists'
            };
            jQuery.post(ajaxurl, data, function(response) {

                if (response === 'yes') {

                    if (confirm('Backup Already Exist! Do You Want To Replace It?')) {
                        Submit_Form('process_backup');
                    }

                } else {
                    Submit_Form('process_backup');
                }

            });
            return false;
        });

        jQuery(".btn-move").live("click", function() {
            Submit_Form('process_transfer');
            return false;
        });

        jQuery(".btn-restore").live("click", function() {
            Submit_Form('process_restore');
        });



    });

    function Submit_Form(task) {
        jQuery('#task').val(task);
        jQuery('.mig-form').submit();
    }

    function checkProgress() {
        var data = {
            action: 'move_progress',
            server: window.serverIP
        };
        jQuery.ajax(ajaxurl, {
            data: data,
            success: function(response) {

                jQuery('.im').attr('width', response);
                if (response.trim() === '100%') {
                    enableRestore();
                } else {
                    setTimeout(checkProgress, 3000);
                }

            },
            error: function() {
                setTimeout(checkProgress, 3000);
            }
        });
    }

    function jsonpHandler() {
    }

    function sendLog(step) {
        try {
            jQuery.ajax('http://www.cloudways.com/en/migrator-download.php?callback=jsonpHandler&app=wordpress&action=install&step=' + step, {
                crossDomain: true,
                dataType: 'jsonp'
            });
        } catch (exc) {
        }
    }
    function enableRestore() {
        jQuery('.message-box').text('Backup Files moved to destination server');
        jQuery('#backup').addClass('opacity');
        jQuery('#move').addClass('opacity');
        jQuery('#restore').removeClass('opacity');
        jQuery('#backup_done').css('display', 'block');
        jQuery('#move_done').css('display', 'block');
        sendLog('restore');
    }
</script>