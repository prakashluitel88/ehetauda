<?php

error_reporting(E_ERROR);

include('Net/SFTP.php');
include('sftp.php');
$ftpHelper = new SFTP();

include('sftp_test.php');
$ftpHelper = new SFTPTest();

switch ($_REQUEST['action']) {
    case 'list':
        echo json_encode(array('status' => TRUE, 'dir' => $_REQUEST['dir'], 'entries' => $ftpHelper->dirList($_REQUEST['dir'])));
        break;

    case 'file':
        echo json_encode(array('status' => TRUE, 'content' => $ftpHelper->getFile($_REQUEST['name'])));
        break;

    default:
        echo json_encode(array('status' => TRUE, 'dir' => '/', 'entries' => $ftpHelper->dirList('.')));
}
?>