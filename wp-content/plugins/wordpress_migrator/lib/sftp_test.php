<?php

class SFTPTest {

    function dirList($dir) {
        return array(
            array('name' => 'first.py', 'type' => 1, 'permissions' => '33188', 'size' => '1234'),
            array('name' => 'uploads', 'type' => 2, 'permissions' => '33188', 'size' => '1234'),
            array('name' => 'second.py', 'type' => 1, 'permissions' => '33188', 'size' => '1234'),
            array('name' => 'third.py', 'type' => 1, 'permissions' => '33188', 'size' => '1234')
        );
    }

    function getFile($name) {
        return 'some file content';
    }

    function putFile($name, $content) {
        $this->sftp->put($name, $content);
        echo 'done';
    }

}