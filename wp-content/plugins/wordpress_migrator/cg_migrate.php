<?php

/**
 * Plugin Name: CG Migrator
 * Plugin URI: http://cloudways.com/
 * Description: Migrate your wordpress site to Cloudways server.
 * Version: 1.0
 * Author: Najmus Saqib
 * License: A "Slug" license name e.g. GPL2
 */
function cgmigrator_menu() {
    add_options_page('CG Migrator', 'CG Migrator', 'manage_options', 'cgmigrator-menu', 'cgmigrator_options');
}

add_action('admin_menu', 'cgmigrator_menu');

function cgmigrator_options() {
    include('cgmigrator-admin.php');
}

add_action('wp_ajax_backup_exists', 'backup_exists_callback');
add_action('wp_ajax_move_progress', 'move_progress_callback');

function backup_exists_callback() {
    if (file_exists("../wordpress_site_backup.zip")) {
        echo 'yes';
    } else {
        echo 'no';
    }
    unlink('wordpress_site_backup.zip');

    die(); // this is required to return a proper result
}

function move_progress_callback() {
    require plugin_dir_path(__FILE__) . 'cg_functions.php';
    $submiturl = 'http://' . validateServerURL($_REQUEST['server']) . '/restore.php?action=progress&' . rand(100, 1000) . '=' . rand(100, 1000);
    $result = callRemoteURL($submiturl);
    echo reset(array_filter(preg_split("/\D+/", $result))) . '%';
    die();
}

?>