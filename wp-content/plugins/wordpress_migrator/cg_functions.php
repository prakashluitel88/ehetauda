<?php

function displayMsg($msg, $type) {
    return "<p class='message-box $type'>$msg</p>";
}

function createRestoreFile($newURL) {
    $contents = file_get_contents(plugin_dir_path(__FILE__) . 'restore.php');
    $contents = str_replace('NEW_URL', $newURL, $contents);
    file_put_contents('restore.php', $contents);
}

function Run_Process_Backup() {
///////  Backup Configuration
    global $table_prefix;
    global $wp_version;
    $config_array = array();
    $config_array['DB_NAME'] = DB_NAME;
    $config_array['DB_USER'] = DB_USER;
    $config_array['DB_PASSWORD'] = DB_PASSWORD;
    $config_array['DB_HOST'] = DB_HOST;
    $config_array['table_prefix'] = $table_prefix;
    $config_array['version'] = $wp_version;
    $config_array['site_url'] = get_home_url();
    file_put_contents('../settings.txt', serialize($config_array));

    include( plugin_dir_path(__FILE__) . 'backup_helper.php');
    include( plugin_dir_path(__FILE__) . 'core.php');
    $backupHelper = new BackupHelper();
    $status = $backupHelper->backup();
    if ($status) {
//if(exec("tar -czvf wordpress_site_backup.zip ./ --exclude='wp-config.php'"))
        echo displayMsg('Backup Created Successfully', 'ok');
        ?>
        <script type="text/javascript">
            jQuery('#backup').addClass('opacity');
            jQuery('#move').removeClass('opacity');
            jQuery('#backup_done').css('display', 'block');
        </script>
        <?php

    } else {
        //print_r($output);
        echo displayMsg('Command has been disabled by host', 'error');
        exit();
    }

//  Delete Files Of No Use
    @unlink('../settings.txt');
    @unlink('../wordpress_db_backup.sql');
}

function runRestoreProcess($data) {

    if (!_iscurlinstalled()) {
        echo displayMsg('Error:Either CURL is Disabled Or Not Installed', 'error');
        exit();
    }

    extract($data);
    $submiturl = "$server_name/restore.php";
    $ch = curl_init($submiturl);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); // return into a variable
    $result = curl_exec($ch);
    $statusCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
    curl_close($ch);

    if ($statusCode == 200) {
//echo "CURL Status : Working ";
//echo "Return Data :";
//echo $result;
        echo displayMsg('Restoration Completed Successfully', 'ok');
        ?>
        <script type="text/javascript">
            jQuery('#backup').addClass('opacity');
            jQuery('#move').addClass('opacity');
            jQuery('#restore').addClass('opacity');
            jQuery('#backup_done').css('display', 'block');
            jQuery('#move_done').css('display', 'block');
            jQuery('#restore_done').css('display', 'block');
        </script>
        <?php

    } else {
        echo displayMsg('Error:Restoration File is missing on Destination Server', 'error');
    }
}

function transferFile($file, $serverURL, $user_name, $password, $directory_path) {
    define('NET_SFTP_LOGGING', NET_SFTP_LOG_SIMPLE);

    $serverURL = str_replace("http://", "", $serverURL);
    $serverURL = str_replace("/", "", $serverURL);

    $remote_file = $directory_path . '/' . $file;
    $local_file = $file;

    $sftp = new Net_SFTP($serverURL);
    if (!$sftp) {
        echo displayMsg('Connection Problems Occured', 'error');
        ?>
        <script type="text/javascript">
            jQuery('#backup').addClass('opacity');
            jQuery('#move').removeClass('opacity');
            jQuery('#backup_done').css('display', 'block');
        </script>
        <?php

        exit();
    }

    $login_result = $sftp->login($user_name, $password);
    if (!$login_result) {
        echo displayMsg('Login Problems Occured', 'error');
//echo $sftp->getSFTPLog();
        ?>
        <script type="text/javascript">
            jQuery('#backup').addClass('opacity');
            jQuery('#move').removeClass('opacity');
            jQuery('#backup_done').css('display', 'block');
        </script>
        <?php

        exit();
    }

    $moverResult = $sftp->put($remote_file, $local_file, NET_SFTP_LOCAL_FILE);
    return $moverResult;
}

function _iscurlinstalled() {
    if (in_array('curl', get_loaded_extensions())) {
        return true;
    } else {
        return false;
    }
}

function validateServerURL($ip) {
    $ip = str_replace("http://", "", $ip);
    return str_replace("/", "", $ip);
}

function callRemoteURL($url) {
    $ch = curl_init($url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); // return into a variable
    curl_setopt($ch, CURLOPT_REFERER, $_SERVER['HTTP_HOST']);
    curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/4.0 (compatible; MSIE 5.01; Windows NT 5.0)');
    curl_setopt($ch, CURLOPT_TIMEOUT, 500);
    $result = curl_exec($ch);
    //var_dump($result);
    //var_dump(curl_error($ch));
    //var_dump(curl_getinfo($ch));
    if (!curl_error($ch))
        return $result;
    curl_close($ch);
}
?>