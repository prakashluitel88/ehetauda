<?php
/**
 * The Sidebar containing the main widget areas.
 */
?>

	<div id="secondary" class="widget-area" role="complementary">

		<?php if ( ! dynamic_sidebar( 'sidebar-1' ) ) : ?>

			<aside id="search" class="widget widget_search">

				<?php get_search_form(); ?>

			</aside>

			<aside id="archives" class="widget">

				<h2 class="widget-title"><?php _e( 'Archives', 'zerif-lite' ); ?></h2>
				<ul>
					<?php wp_get_archives( array( 'type' => 'monthly' ) ); ?>
				</ul>

			</aside>

			<aside id="meta" class="widget">

				<h2 class="widget-title"><?php _e( 'Meta', 'zerif-lite' ); ?></h2>

				<ul>
					<?php wp_register(); ?>
					<li><?php wp_loginout(); ?></li>
					<?php wp_meta(); ?>
				</ul>

			</aside>

		<?php endif; ?>


<aside id="nav_menu-3" class="widget widget_nav_menu"><div class="menu-main-menu-container">
<?php
if(is_category()) {

	$breakpoint = 0;
	$thiscat = get_term( get_query_var('cat') , 'category' );
	$subcategories = get_terms( 'category' , 'parent='.get_query_var('cat') );

	if(empty($subcategories) && $thiscat->parent != 0) {
		$subcategories = get_terms( 'category' , 'parent='.$thiscat->parent.'' );
	}

	$items='';
	if(!empty($subcategories)) {
		foreach($subcategories as $subcat) {
			if($thiscat->term_id == $subcat->term_id) $current = ' current-cat'; else $current = '';
			$items .= '
			<li class="cat-item cat-item-'.$subcat->term_id.$current.'">
				<a href="'.get_category_link( $subcat->term_id ).'" title="'.$subcat->description.'">'.$subcat->name.' ('.$subcat->count.' posts)</a>
			</li>';
		}
		echo "<ul>$items</ul>";
	}
	unset($subcategories,$subcat,$thiscat,$items);
}
?>
</div></aside>
	</div><!-- #secondary -->